let dc = document
let htmlEditor = dc.querySelector('#html-editor')
let output = dc.querySelector('.output')
let ace_html = ace.edit(htmlEditor);
let html_text
let placeholder_text = `<style>div{width:20px; height:20px; background-color:red; position:relative; top:30vh;}main{background:#333; height:100vh; width:100vh;}</style>
<div class="cas"></div>`

// Create shadow DOM from <template> in HTML file
output.attachShadow({mode:'open'}).appendChild(tpl.content)
// Set container variable after shadow DOM init
let container = document.querySelector('.output').shadowRoot.querySelector('cascade-root main');

// Init by decoding the URL
decode()
encode()

// // ACE editor settings
ace_html.setTheme("ace/theme/tomorrow_night_bright");
ace_html.getSession().setMode("ace/mode/html");
ace_html.setOptions({useSoftTabs: true, wrap:true, tabSize:"2", enableBasicAutocompletion: true, enableSnippets: true, enableLiveAutocompletion: false})
ace_html.setShowPrintMargin(false)
ace_html.commands.removeCommand('gotoline')

ace_html.on("change", function(){
  html_text = ace_html.getValue();
  encode()
})

// Text to URL and preview
function encode(){
  html_text = ace_html.getValue();
  let encoded = window.btoa(html_text);
  let currentURL = document.location.href;
  let newURL = currentURL.split('#')[0]+"#"+encoded
  document.location.href = newURL
  let content = html_text;
  container.innerHTML = content
}

// URL to editor
function decode(){
  let url = document.location.href;
  let encoded = url.split('#')[1]
  if (encoded == undefined || encoded == "" || url == "#") {
    ace_html.session.setValue(placeholder_text)
  }else{
    ace_html.session.setValue(window.atob(encoded))
  }
}

window.addEventListener("DOMContentLoaded", (event) => {
  console.log('loaded');
  let c = new Cascade(container)
});

function clearEditor(){
  ace_html.session.setValue("")
  encode()
}
function setDefault(){
  ace_html.session.setValue(placeholder_text)
}