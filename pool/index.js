const express = require('express');
const socketio = require('socket.io');
const path = require('path');
const app = express();
const server = require('http').Server(app);
const io = socketio(server);
const ip = require("ip");
const port = "8082";

io.on('connect', onConnect);

let performerRoom;
function onConnect(socket){
  const id = socket.id;

  socket.once("joinRoom", function(room) {
    // socket.emit('stopEverything');

    if (!(/[^\w.]/.test(room))) {
      socket.join(room);
    }
    if (room == 'performer') {
      performerRoom = io.sockets.adapter.rooms.get('performer');
      console.log('A performer joined');
      if (performerRoom) {
        // console.log([...performerRoom][0]);

      }
    } else if(room == 'audience'){
      console.log('An audience member joined');
      performerRoom = io.sockets.adapter.rooms.get('performer');
      // console.log(performerRoom);
      if (performerRoom) {
        console.log('new connection');
        socket.to([...performerRoom][0]).emit('newConnection');
        // console.log([...performerRoom][0]);
      }
    }
    // console.log("New" + room, io.sockets.adapter.rooms.get(room));
    // only allow certain characters in room names
    // to prevent messing with socket.io internal rooms
  });

  socket.on('disconnect', function () {
    console.log('disconnect');
    performerRoom = io.sockets.adapter.rooms.get('performer');
    if (!performerRoom) {
      // if there is no more performer, stop all cascades
      io.emit('stopEverything');
    }
  });

  socket.on('newCascadeIO', function(newCascadeObj) {
    // doSomething
    console.log("newCascade");
    socket.to('performer').emit("newCascadeIO", newCascadeObj, true);
    socket.to('audience').emit("newCascadeIO", newCascadeObj, false);
  });

  socket.on('update', function(update) {
    // doSomething
    console.log("update el");
    socket.broadcast.emit("update", update);
  });

  socket.on('add', function(obj) {
    console.log("add", obj.indexOfCascade, socket.id);
    socket.broadcast.emit("add", obj);
  });

  socket.on('rm', function(obj) {
    console.log("rm");
    socket.broadcast.emit("rm", obj);
  });

  socket.on('stop', function(indexOfCascadeObj) {
    // console.log(socket.id, 'stop');
    socket.broadcast.emit("stop", indexOfCascadeObj);
  });

  socket.on('start', function(indexOfCascadeObj) {
    console.log("start");
    socket.broadcast.emit("start", indexOfCascadeObj);
  });
}

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/app/audience.html');
});
app.get('/js/*.js', function(req, res) {
  res.sendFile(path.join(__dirname, '..', req.url), (err) => {
    if (err) {
      res.sendFile(path.join(__dirname, 'app', req.url));
    }
  });
});
app.get('/website/*', function(req, res) {
  res.sendFile(path.join(__dirname, '..', req.url));
});

app.get('/css/*.css', function(req, res) {
  res.sendFile(path.join(__dirname, '..', req.url), (err) => {
    if (err) {
      res.sendFile(path.join(__dirname, 'app', req.url));
    }
  });
});

app.get('/performer', function(req, res) {
  res.sendFile(__dirname + '/app/performer.html');
});

server.listen(port);

console.log('Welcome to CASCADE ~P~O~O~L~');
console.log('~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~');
console.log('Local Performers URL  : http://localhost:'+port+'/performer');
console.log('Local Audience URL    : http://localhost:'+port+' ');
console.log('Remote Performers URL : http://'+ip.address()+':'+port+'/performer');
console.log('Remote Audience URL   : http://'+ip.address()+':'+port+'');
console.log('~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~');
