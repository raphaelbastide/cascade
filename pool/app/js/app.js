const socket = io();
let cascadeArray = [];


// Socket Io

socket.on('start', function(index) {
  cascadeArray[index].start(0, false);
  // false so that no event is sent, otherwise there will be a recursion problem

})
socket.on('stop', function(index) {
  cascadeArray[index].stop(false);
  // false so that no event is sent, otherwise there will be a recursion problem
})

socket.on('add', function(obj) {
  console.log('add');
  let cascade = cascadeArray[obj.indexOfCascade];
  if (cascade) {
    let parent;

    if (obj.pathToParent) {
      parent = cascade.els[obj.pathToParent[0]];
      for (var i = 1; i < obj.pathToParent.length; i++) {
        parent = parent.children[obj.pathToParent[i]]
      }
    } else {
      parent = cascade.rootNode;
    }
    let child = d.createElement(obj.newNode.tagType);
    jsObjToHtmlNode(obj.newNode, child);
    child.dataset.socketIO = false;
    if (parent.htmlNode) {
      parent.htmlNode.appendChild(child);
    } else {
      parent.appendChild(child);
    }
  }
})

socket.on('rm', function(obj) {
  let cascade = cascadeArray[obj.indexOfCascade];
  cascade.rm(obj.params.id, false);
})

socket.on('update', function(update) {
  console.log('update');
  let cascade = cascadeArray[update.indexOfCascade]

  if (cascade) {
    let elToUpdate;
    if (update.pathElToUpdate) {
      elToUpdate = cascade.els[update.pathElToUpdate[0]];
      for (var i = 1; i < update.pathElToUpdate.length; i++) {
        elToUpdate = elToUpdate.children[update.pathElToUpdate[i]]
      }
      elToUpdate.htmlNode.dataset.socketIO = false;
      jsObjToHtmlNode(update.newEl, elToUpdate.htmlNode);
    } else {

      elToUpdate = cascade.rootNode;
      elToUpdate.dataset.socketIO = false;
      jsObjToHtmlNode(update.newEl, elToUpdate);
    }


  }
})

socket.on('newCascadeIO', function(simplifiedCascadeObj, isPerformer) {
  console.log("newCascade");
  // console.log(simplifiedCascadeObj);
  // Check if already there, in that case replace it
  let c;
  if (cascadeArray.length > 0 && d.querySelector(simplifiedCascadeObj.htmlStructure[0].tagType + '#c' + simplifiedCascadeObj.htmlStructure[0].classes.map(i => '.' + i).join(''))) {
    // If the new Cascade already exists in this page. Relies only on the classlist, which isn't the best :(
    console.log('Replace');

    let classListArray = simplifiedCascadeObj.htmlStructure[0].classes;
    c = cascadeArray.find(cItem => compare(classListArray, [...cItem.rootNode.classList]));
    c.stop();
    c.rootNode.remove();
    cascadeArray.splice(cascadeArray.indexOf(c), 1)
    recreateHtmlStructure(simplifiedCascadeObj.htmlStructure);

    function compare(array1, array2) {
      return array1.length === array2.length && array1.every((value, index) => value === array2[index])
    }
  } else {
    recreateHtmlStructure(simplifiedCascadeObj.htmlStructure);
  }

  parseStyleSheet(simplifiedCascadeObj.styleSheetRules);
  c = new Cascade('.rootNode', {
    newCascadeEvent: false
  });
  let rootNode = d.querySelector(simplifiedCascadeObj.htmlStructure[0].tagType + '.rootNode') // this could be simplifed if cascade can take on html node as well as strings.
  rootNode.classList.remove('rootNode');
  if (isPerformer) {
    addEventListenerToCascade(c);
  }
  cascadeArray.push(c);
});

function addEventListenerToCascade(cascadeObj) {

  cascadeObj.on('stop', function(e) {
    socket.emit('stop', cascadeArray.indexOf(cascadeObj));
  })
  cascadeObj.on('start', function(e) {
    socket.emit('start', cascadeArray.indexOf(cascadeObj));
  })

  cascadeObj.on('rm', function(e) {
    console.log('rm');
    socket.emit('rm', {
      indexOfCascade: cascadeArray.indexOf(cascadeObj),
      params: e.detail.params,
    });
  })

  cascadeObj.on('add', function(e) {
    let pathToParent;
    if (e.detail.parent != cascadeObj.rootNode) {
      pathToParent = findInRecursiveArray(cascadeObj.els, e.detail.parent)[0].path;
      pathToParent.reverse();
    }


    let newNode = htmlNodeToJsObj(e.detail.newNode)
    if (e.detail.newNode.dataset.socketIO != 'false') {
      socket.emit('add', {
        indexOfCascade: cascadeArray.indexOf(cascadeObj),
        pathToParent: pathToParent,
        newNode: newNode
      });
    } else {
      e.detail.newNode.dataset.socketIO = undefined;
    }
  })

  cascadeObj.on('update', function(e) {
    let elToUpdate = e.detail.element;
    let newEl;
    let pathElToUpdate;
    if (elToUpdate != cascadeObj.rootNode) {
      pathElToUpdate = findInRecursiveArray(cascadeObj.els, elToUpdate)[0].path;
      pathElToUpdate.reverse();
      newEl = htmlNodeToJsObj(e.detail.element.htmlNode);

      if (e.detail.element.htmlNode.dataset.socketIO != 'false') {
        socket.emit('update', {
          indexOfCascade: cascadeArray.indexOf(cascadeObj),
          pathElToUpdate: pathElToUpdate,
          newEl: newEl
        });
      } else {
        e.detail.element.htmlNode.dataset.socketIO = undefined;
      }

    } else {
      newEl = htmlNodeToJsObj(e.detail.element);
      console.log(newEl);
      if (e.detail.element.dataset.socketIO != 'false') {
        socket.emit('update', {
          indexOfCascade: cascadeArray.indexOf(cascadeObj),
          pathElToUpdate: pathElToUpdate,
          newEl: newEl
        });
      } else {
        e.detail.element.dataset.socketIO = undefined;
      }
    }


  })
}


// Rendering (or composing function);

function sendCascade(cascadeObj) {
  let simplifiedCascadeObj = {};
  let rootNode = cascadeObj.rootNode;
  let styleSheetRules = cascadeObj.getDocumentCssRules();
  simplifiedCascadeObj.styleSheetRules = cssStyleSheetToJsObj(styleSheetRules);
  simplifiedCascadeObj.htmlStructure = [];
  let rootNodeObj = htmlNodeToJsObj(rootNode);
  // console.log(cascadeObj);
  cascadeObj.els.forEach((childEl, i) => {
    recursiveParserCsdElToJsObj(rootNodeObj, childEl)
  });
  simplifiedCascadeObj.htmlStructure.push(rootNodeObj);
  addEventListenerToCascade(cascadeObj);
  // console.log(simplifiedCascadeObj);
  socket.emit('newCascadeIO', simplifiedCascadeObj);
}

function recursiveParserCsdElToJsObj(parent, node) {
  let obj = htmlNodeToJsObj(node["htmlNode"]);
  parent.children.push(obj);
  for (var i = 0; i < node.children.length; i++) {
    recursiveParserCsdElToJsObj(obj, node.children[i])
  }
}

function htmlNodeToJsObj(htmlNode) {
  let obj = {};
  obj.tagType = htmlNode.tagName;
  obj.classes = [...htmlNode.classList];
  // obj.dataset = {
  //   ...htmlNode.dataset
  // };
  obj.style = cssStyleDeclarationToJsObj(htmlNode.style);
  obj.children = [];
  return obj;
}

function cssStyleSheetToJsObj(styleSheetDeclaration) {
  // CSSStyleDeclaration https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration
  let objArray = [];
  styleSheetDeclaration.forEach((item, i) => {
    let obj = {};
    obj['selectorText'] = item.selectorText;
    obj['style'] = cssStyleDeclarationToJsObj(item.style);
    objArray.push(obj);
  });
  return objArray;
}

function cssStyleDeclarationToJsObj(styleDeclaration) {
  // CSSStyleDeclaration https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration
  let obj = {};
  for (var i = 0; i < styleDeclaration.length; i++) {
    obj[styleDeclaration[i]] = styleDeclaration.getPropertyValue(styleDeclaration[i]);
  }
  return obj;
}

// Parsing functions

function recreateHtmlStructure(htmlStructure) {
  let rootNodeClone = d.createElement(htmlStructure[0].tagType);
  jsObjToHtmlNode(htmlStructure[0], rootNodeClone);
  rootNodeClone.classList.add('rootNode');
  htmlStructure[0].children.forEach((childJsObj, i) => {
    recursiveParserJsObjToHtml(rootNodeClone, childJsObj)
  });
  if (htmlStructure[0].tagType == "BODY") {
    d.body = rootNodeClone;
  } else {
    d.body.appendChild(rootNodeClone);
  }
}

function parseStyleSheet(styleSheetObj) {
  let style = document.createElement("style");
  document.head.appendChild(style); // must append before you can access sheet property
  let styleSheet = style.sheet;

  styleSheetObj.forEach((item, i) => {
    let cssString = item.selectorText + "{";
    Object.keys(item.style).forEach(key => {
      cssString += key + ":" + item.style[key] + ";"
    });
    cssString += "}"
    try {
      styleSheet.insertRule(cssString, styleSheet.cssRules.length);
    } catch (e) {

    }
  });
}

function recursiveParserJsObjToHtml(parent, jsObj) {
  let node = d.createElement(jsObj.tagType);
  jsObjToHtmlNode(jsObj, node);
  parent.appendChild(node);
  for (var i = 0; i < jsObj.children.length; i++) {
    recursiveParserJsObjToHtml(node, jsObj.children[i])
  }
}

function jsObjToHtmlNode(jsObj, node) {
  jsObj.classes.forEach((cssClass, i) => {
    node.classList.add(cssClass)
  });
  Object.keys(jsObj.style).forEach(property => {
    node.style.setProperty(property, jsObj.style[property])
  });
}
