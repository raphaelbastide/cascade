
d.addEventListener("newCascade", function(e) {
  let newCascadeObj = e.detail.cascade;
  cascadeArray.push(newCascadeObj);
  sendCascade(newCascadeObj);
})

let c = new Cascade("body");
socket.on('connect', function() {
  socket.emit("joinRoom", "performer");
});

socket.on('newConnection', function(message) {
  console.log("here");
  cascadeArray.forEach((cascade, i) => {
    sendCascade(cascade);
  });
});
