<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>cascade</title>
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="website/img/favicon.svg">
    <link rel="stylesheet" href="website/website.css">
    <link rel="stylesheet" href="website/fonts/stylesheet.css">
    <meta name="description" content="Livecoding with CSS" />
    <meta name="keywords" content="art, netart, sound, CSS, livecoding" />
    <meta name="twitter:card" content="summary" />
    <meta property="twitter:creator" content="@raphaelbastide" >
    <meta property="og:title" content="Cascade" />
    <meta property="twitter:url" content="https://raphaelbastide.com/cascade" />
    <meta property="og:url" content="https://raphaelbastide.com/cascade" />
    <meta property="og:image" content="https://raphaelbastide.com/cascade/website/img/favicon.png" />
    <meta name="twitter:image" content="https://raphaelbastide.com/cascade/website/img/favicon.png" />
  </head>
  <body>
    <img src="website/img/logo.svg" alt="">
    <div>
      <p>Cascade is a live-coding environment <br> It runs in the web browser, turning CSS rules into sound</p>
    </div>
    <div>
      <p><a href="https://gitlab.com/raphaelbastide/cascade">Code on Gitlab</a> <a href="https://peertube.swrs.net/video-channels/cascade/videos">Videos</a> <a href="https://discord.gg/Av5yNJH3Am">Discussion</a></p>
    </div>
    <div class="remote-content">
      <div class="readme">
        <?php
        require_once 'website/functions.php';
        $readme = file_get_contents('README.md');
        $Parsedown = new Parsedown();
        echo $Parsedown->text($readme);
        ?>
      </div>
      <div class="lists">
        <div class="toc-list">
          <h2>Table of content</h2>
          <ul></ul>  
        </div>
        <div class="demo-list">
          <h2>Demos</h2>
          <?php
          $directory = "demos";
          $demos = rglob($directory."/{*.html}", GLOB_BRACE);
          $demos = array_reverse($demos);
          if (!empty($demos)) {
            echo "<ul>";
            foreach ($demos as $demo) {
              echo "<li><a href='".$demo."'>".$demo."</a></li>";
            }
            echo "</ul>";
          }
        ?>
        </div>
      </div>
    </div>
    <script>
        let titles = document.querySelectorAll('.readme h2')
        let tocBox = document.querySelector('.toc-list ul')
        titles.forEach(title => {
          let content = title.innerHTML
          title.setAttribute('id', slugify(content))
          let link = document.createElement('a')
          let li = document.createElement('li')
          link.setAttribute('href', '#'+slugify(content))
          link.innerText = content
          li.appendChild(link)
          tocBox.appendChild(li)
        });

        function slugify(text){
          return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
        }

        const el = document.querySelector('.lists');
        let elTop = el.getBoundingClientRect().top
        let elTopDiff = el.getBoundingClientRect().top - document.body.getBoundingClientRect().top;
        function placeLists(){
          let bTop = document.body.getBoundingClientRect().top
          if (document.documentElement.scrollTop > elTopDiff){
            el.style.position = 'fixed';
            el.style.top = "27px"; 
            el.style.left = "50%"; 
            el.style.width = "calc(50vw - 29px)"
          }else{
            el.style.position = 'static';
            el.style.top = '0';
            el.style.removeProperty('width');
          }
        }
        window.addEventListener('scroll', function(){
          placeLists()
        });
    </script>
  </body>
</html>
