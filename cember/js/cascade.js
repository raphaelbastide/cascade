// Cascade v.0.5
// https://raphaelbastide.com/cascade
let d = document,
  b = d.body,
  tempo, wh, ww, bpm,
  activated = isMute = false,
  isPlaying = false,
  metronome = false,
  blankPageMessage = true,
  tick = timerMin = 0,
  elsList = insList = colors = [],
  interval,
  panner,
  defaultStyle = "width:100px; height:200px; background:var(--c0); position:absolute; bottom:40vh; left:40vw;",
  els = [];
let debugTime = false;
let allowedCssProperties = ['opacity', 'display', 'visibility', 'background-color', 'border-top-left-radius', 'border-top-right-radius', 'border-bottom-right-radius', 'border-bottom-left-radius', 'width', 'height', 'border-top-width', 'border-right-width', 'border-bottom-width', 'border-left-width', ];

class Element {
  constructor(htmlNode, cssStyleDeclaration) {
    this.htmlNode = htmlNode;
    this.cssProperties = this.setCssProperties(cssStyleDeclaration);
    this.children = [];
    this.computedProperties = this.setComputedProperties();
    }
  setCssProperties(css) {
    return this.filterCssProperties(this.cssStyleDeclarationToJsObj(css), allowedCssProperties)
  }
  cssStyleDeclarationToJsObj(styleDeclaration) {
    // CSSStyleDeclaration https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration
    let obj = {};
    for (var i = 0; i < styleDeclaration.length; i++) {
      obj[styleDeclaration[i]] = styleDeclaration.getPropertyValue(styleDeclaration[i]);
    }
    return obj;
  }
  filterCssProperties(cssProperties, allowedCssProperties) {
    // Modified version of https://stackoverflow.com/questions/38750705/filter-object-properties-by-key-in-es6
    // Filters the array so that only the relevant p
    let filteredArray = Object.keys(cssProperties).filter(key => allowedCssProperties.includes(key))
    return filteredArray.reduce((obj, key) => {
      obj[key] = cssProperties[key];
      return obj;
    }, {});
  }

  setComputedProperties() {
    let obj = {}
    obj.relativePos = this.getRelativePos(this.htmlNode);
    obj.ratio = getRatio(parseInt(this.cssProperties.width), parseInt(this.cssProperties.height))
    obj.surface = parseInt(this.cssProperties.width) * parseInt(this.cssProperties.height)
    obj.insID = getVarNbr(this.cssProperties['background-color'])
    obj.currentInstrument = obj.insID ? insList[obj.insID] : insList[obj[0]]
    obj.insSynthType = instruments[obj.insID].synth
    obj.volumeCorrection = instruments[obj.insID].volumeCorrection
    obj.note = Math.min(300, Math.round(obj.relativePos.y) + 30)
    obj.borderTW = parseInt(this.cssProperties['border-top-width'])
    obj.borderRW = parseInt(this.cssProperties['border-right-width'])
    obj.borderBW = parseInt(this.cssProperties['border-bottom-width'])
    obj.borderLW = parseInt(this.cssProperties['border-left-width'])
    obj.radTL = parseInt(this.cssProperties['border-top-left-radius'])
    obj.radTR = parseInt(this.cssProperties['border-top-right-radius'])
    obj.radBR = parseInt(this.cssProperties['border-bottom-right-radius'])
    obj.radBL = parseInt(this.cssProperties['border-bottom-left-radius'])
    obj.totalBorderRadius = (obj.radTL + obj.radTR + obj.radBR + obj.radBL) / 4;
    obj.probability = this.cssProperties['opacity'] * 10
    obj.duration = obj.totalBorderRadius == 0 ? .2 : obj.totalBorderRadius / 20
    // problem with negative duration;
     return obj;
  }

  getRelativePos(element) {
    let pos = element.getBoundingClientRect()
    let parentPos = element.parentNode.getBoundingClientRect()
    let relativePos = []
    relativePos.top = pos.top - parentPos.top,
      relativePos.right = pos.right - parentPos.right,
      relativePos.bottom = pos.bottom - parentPos.bottom,
      relativePos.left = pos.left - parentPos.left;
    let propPosX = (pos.left - parentPos.left) * 100 / ww
    let propPosY = pos.top * 100 / wh
    return {
      x: propPosX,
      y: propPosY
    }
  }
}


createStartBtn();
// initAndStop(100); // for testing

// Init only after start button has been pressed (to allow sond on page)
function init() {
  console.info('Welcome to Cascade')
  reportWindowSize()
  window.onresize = reportWindowSize
  createInstruments()
  makeColorList()
  initTimer()
  previousCssRules = document.styleSheets[0].rules;

  let elNodes = d.querySelectorAll("body > [data-id]");
  elNodes.forEach((elNode, i) => {
    initEl("root", elNode);
  });
  addMutationObserverChildren("root", b);
  start();
}

function initEl(parent, elNode) {
  let el = new Element(elNode, window.getComputedStyle(elNode));
  addMutationObserverEl(el, elNode);
  addMutationObserverChildren(el, elNode)
  if (parent == "root") {
    els.push(el);
  } else {
    parent.children.push(el)
  }
  let children = elNode.querySelectorAll("[data-id]");
  children.forEach((childNode, i) => {
    initEl(el, childNode);
  });


}

function addMutationObserverChildren(parent, parentNode) {
  // watches for new nodes added to the page and adds them to the cascade
  let config = {
    childList: true,
  };

  let mutationCallbackChildren = function(mutationsList) {
    mutationsList.forEach((mutation, i) => {
      if (mutation.type == 'childList') {
        mutation.addedNodes.forEach((newNode, i) => {
          if (newNode.dataset.id) {
            initEl(parent, newNode);
          }
        });

        mutation.removedNodes.forEach((deletedNode, i) => {
          if (deletedNode.dataset.id) {
            let indexOfNode = els.indexOf(els.filter(el => el.htmlNode == deletedNode)[0]);
            if (indexOfNode != -1) {
              els.splice(indexOfNode, 1);
            }
          }
        });
      }
    });
  };
  let childrenObserver = new MutationObserver(mutationCallbackChildren);
  childrenObserver.observe(parentNode, config);
}

function addMutationObserverEl(el, targetNode) {
  let config = {
    attributes: true,
    // attributeFilter: ["style"],
  };

  let mutationCallback = function(mutationsList) {
    mutationsList.forEach((mutation, i) => {
      // console.log(mutation);
      // Since it's not possible to know which css property was changed, all of the css properties are reloaded
      el.cssProperties = el.setCssProperties(window.getComputedStyle(mutation.target));
      el.computedProperties = el.setComputedProperties();
    });
  };
  let observer = new MutationObserver(mutationCallback);
  observer.observe(targetNode, config);
}

// System functions
function interpret(element) {
  let elProp = element.computedProperties
  let cycle = Number(element.htmlNode.getAttribute('data-cycle'))
  if (element.htmlNode.getAttribute('data-cycle') == undefined) {
    element.htmlNode.setAttribute('data-cycle', 0)
  }

  if (!elProp.delay) {
    // the delay can't be setup at the same time as the other computed properties because it relies on the interval, which itself relies on the bpm
    elProp.delay = Math.round(elProp.relativePos.x * interval / 100) < 0 ? 0 : Math.round(elProp.relativePos.x * interval / 100);

  }

  // All of these could be setup once
  element.htmlNode.setAttribute('data-ratio', elProp["ratio"][0] + ":" + elProp["ratio"][1])
  element.htmlNode.setAttribute('data-instrument', elProp["insID"])
  element.htmlNode.setAttribute('data-childs', element.children.length);

  // This could move to computed properties as well
  let euclSteps = Math.max(elProp["ratio"][0], elProp["ratio"][1])
  let euclBeats = Math.min(elProp["ratio"][0], elProp["ratio"][1])
  elProp.panVal = (elProp.borderRW - elProp.borderLW) * 10
  let euclRotation = elProp.borderTW
  let euclPat = generatePattern(euclBeats, euclSteps);
  let probRan = Math.round(Math.random() * 10)
  shiftPat(euclPat, euclRotation)
  if (
    element.cssProperties['display'] !== "none" &&
    element.cssProperties['visibility'] !== "hidden" &&
    (euclPat[cycle - 1]) == 1
  ) {
    setTimeout(function() {
      if (debugTime) {
        if (typeof timeTracking === "function") {
          timeTracking(element, arguments[0], elProp.delay);
        }else{
          console.info('Cannot debug: deubugTime is `true`, but debug.js is not linked to this page.');
        }
      }

      if (elProp.probability >= probRan) {
        if (!isMute && element.children.length == 0) { // not muted & has no child
          // if(elProp.panVal > -10){elProp.panVal = -10}else if(elProp.panVal < -1){elProp.panVal = -1}
          if(elProp.insSynthType !== "none"){
            //  elProp.currentInstrument.connect(panner);
            //  panner.pan.value = elProp.panVal
            // Change volume depending on element surface and volume correction
            elProp.currentInstrument.volume.value = Math.round(elProp.surface / 20000) + elProp.volumeCorrection
            // Limite volume max value to 10db
            if(elProp.currentInstrument.volume.value >= 10){elProp.currentInstrument.volume.value = 10}
          }
      
          if (elProp.insSynthType === "Noise") {
            elProp.currentInstrument.start()
            setTimeout(() => {
              elProp.currentInstrument.stop()
            }, elProp.duration);
          }else if(elProp.insSynthType === "PluckSynth") {
            elProp.currentInstrument.triggerAttack(elProp.note, elProp.duration);
          }else if(elProp.insSynthType === "none") {
            // do nothing :3
          }else{
            elProp.currentInstrument.triggerAttackRelease(elProp.note, elProp.duration);
          }
        }
        activateElement(element.htmlNode);
        // interpret children
        element.children.forEach((child, i) => {
          interpret(child);
        });
      }
    }, elProp.delay, Date.now() /* only used if debugTime */);
  }
  if (cycle >= euclPat.length) {
    cycle = 0
  }
  cycle++
  element.htmlNode.setAttribute('data-cycle', cycle)
}

function nextTick() {
  if (!isPlaying) {
    tick = -1
    return
  }
  // BPM
  if (bpm != getbpm()) {
    bpm = getbpm()
    if (bpm <= 0)
      bpm = 1
    console.info('BPM = ' + bpm);
    interval = Math.round((60 * 1000) / (bpm))
    // sets interval as CSS variable for animation syncing
    b.style.setProperty('--interval', interval / 1000)
  }

  // Interpret all elements
  if (els.length == 0 && blankPageMessage) {
    console.info("No HTML element found, try adding one with add()");
    blankPageMessage = false
  } else if (els.length != 0) {
    blankPageMessage = true;

    if (previousCssRules != document.styleSheets[0].cssRules){
      // fix for issue #47. this is not the prettiest but it should work
      previousCssRules = document.styleSheets[0].cssRules;
      console.log('Stylesheet change / not inline');
      els.forEach((el, i) => {
        el.cssProperties = el.setCssProperties(window.getComputedStyle(el.htmlNode));
        el.computedProperties = el.setComputedProperties();
      });
    }


    els.forEach((el, i) => {
      interpret(el)
    });

  }
  tick++
  interval = Math.round((60 * 1000) / (bpm));
  return setTimeout(function() {
    return nextTick();
  }, interval);
}


// Cascade API
function start(delay) {
  setTimeout(function() {
    if (!isPlaying) {
      isPlaying = true
      nextTick()
      console.info('Cascade started');
    } else {
      console.info('Already started');
    }
  }, delay * 1000);
}

function stop() {
  isPlaying = false
  console.info('Cascade stopped');
  let event = new CustomEvent('stop')
  d.dispatchEvent(event);
}

function add(style = defaultStyle, parentId = "body", copies = 1) {
  let elementsList = []
  if (parentId == "body") {
    parent = b
  } else {
    parent = d.querySelector('[data-id="' + parentId + '"]')
  }
  if (parent == undefined) {
    console.info('Chosen parent element is undefined');
    return
  } else {
    for (var i = 0; i < copies; i++) {
      let el = d.createElement("div")
      let id;
      if (parent == b) {
        id = nextId(parent);
      } else {
        id = parentId + '-' + nextId(parent);
      }

      el.setAttribute('data-id', id);
      el.id = "c-" + id;
      if (style)
        el.style = style
      parent.appendChild(el)
      elementsList.push(el)
      console.info('Added new element ' + id + ' to ' + parentId);
    }
  }
  let event = new CustomEvent('add', {
    detail: {
      elements: elementsList
    }
  })
  // dispatch the event
  d.dispatchEvent(event);
  if (copies == 1){return(event)}
}

function rawStyle(css) {
  let style = d.querySelector('style').innerHTML
  d.querySelector('style').innerHTML = style + "\n" + css
}

function mod(style, id) {
  if (Array.isArray(id)) {
    for (var i = 0; i < id.length; i++) {
      let el = d.querySelector('[data-id="' + id[i] + '"]')
      el.style.cssText += style
    }
  } else {
    let el = d.querySelector('[data-id="' + id + '"]')
    el.style.cssText += style
  }
}

function addCl(cl, id) {
  let el = d.querySelector('[data-id="' + id + '"]')
  el.classList.add(cl)
}

function line(style, nbr=4){
  console.log(nbr);
  let event = add('display:flex; width:90vw; height:20vh; justify-content:space-between; outline:1px dotted gray;' + style)
  let parentID = event.detail.elements[0].dataset.id
  add('width:40px; height:40px; background:red; position:static;', parentID, nbr)
}

function rmCl(cl, id) {
  let el = d.querySelector('[data-id="' + id + '"]')
  el.classList.remove(cl)
}

function clone(style, cloneFromId) {
  let clonedElement = d.querySelector('[data-id="' + cloneFromId + '"]')
  let el = clonedElement.cloneNode(true);
  let id = nextId(b)
  el.setAttribute('data-id', id)
  if (style) {
    el.style.cssText += style
  }
  parent = clonedElement.parentNode
  parent.appendChild(el)
  console.info('Cloned new element ' + id);
  let event = new CustomEvent('clone', {
    detail: {
      element: el
    }
  })
  d.dispatchEvent(event);

}

function rm(id) {
  if (Array.isArray(id)) {
    for (var i = 0; i < id.length; i++) {
      d.querySelector('[data-id="' + id[i] + '"]').remove();
      console.info('Element ' + id[i] + ' removed');
    }
  } else {
    d.querySelector('[data-id="' + id + '"]').remove();
    console.info('Element ' + id + ' removed');
  }
}

function clear() {
  let els = d.querySelectorAll('[data-id]');
  for (var i = 0; i < els.length; i++) {
    els[i].remove()
  }
  console.info('Cleared all elements')
}

function ls() {
  let els = b.querySelectorAll('div')
  for (var i = 0; i < els.length; i++) {
    console.info(els[i].nodeName + ': ' + els[i].getAttribute('data-id'));
  }
}

function mute() {
  isMute = !isMute
}

function dl(fileName) {
  let date = new Date().getTime();
  let html = document.querySelector('html').innerHTML
  if (!fileName) {
    fileName = "cascade-" + date + ".html"
  }
  download(html, fileName, "text/html");
}

function flash(time = 4) {
  createColorTable()
  b.classList.add('flash')
  setTimeout(function() {
    b.classList.remove('flash')
  }, time * 1000);
  return
}

function grid(time = 10) {
  if (typeof(d.querySelector('.c-grid')) != 'undefined'
  && d.querySelector('.c-grid') != null) {
    let gridEl = d.querySelector('.c-grid')
  }else{
    let gridEl = d.createElement('div')
    gridEl.classList.add('c-grid')
    b.appendChild(gridEl)
  }
  b.classList.add('show-grid')
  setTimeout(function() {
    b.classList.remove('show-grid')
  }, time * 1000);
  return
}


function nextId(parent) {
  // counting existing ids
  let existing = parent.querySelectorAll(':scope > [data-id]');
  if (existing.length == 0) {
    return 0
  } else {
    let ids = []
    for (var i = 0; i < existing.length; i++) {
      let splitId = existing[i].getAttribute('data-id').split('-')
      let id = Number(splitId[splitId.length-1]);
      ids.push(id)
    }
    // next id
    return Math.max(...ids) + 1
  }
}

function getbpm() {
  let color = window.getComputedStyle(b).getPropertyValue('background-color');
  let lum = RGBtoHSL(color)['lum']
  return Math.round(lum * 3) + 20 // *2 makes enlarge the bpm range, currently in test
}

function createInstruments() {
  // panner = new Tone.Panner().toDestination();
  instruments.forEach((instrument, index) => {
    let synthType = instrument.synth
    let insOptions = instrument.options;

    if (synthType === "MembraneSynth") {
      insList[index] = new Tone.MembraneSynth(insOptions).toDestination()
    } else if (synthType === "MetalSynth") {
      insList[index] = new Tone.MetalSynth(insOptions).toDestination()
    } else if (synthType === "PluckSynth") {
      insList[index] = new Tone.PluckSynth(insOptions).toDestination()
    } else if (synthType === "AMSynth") {
      insList[index] = new Tone.AMSynth(insOptions).toDestination()
    } else if (synthType === "DuoSynth") {
      insList[index] = new Tone.DuoSynth(insOptions).toDestination()
    } else if (synthType === "MonoSynth") {
      insList[index] = new Tone.MonoSynth(insOptions).toDestination()
    } else if (synthType === "Noise") {
      insList[index] = new Tone.Noise(insOptions).toDestination()
    } else if (synthType === "PolySynth") {
      insList[index] = new Tone.PolySynth(insOptions).toDestination()
    } else if (synthType === "none") {
      insList[index] = ""
    } else { // plain synth
      insList[index] = new Tone.Synth(insOptions).toDestination()
    }
  });
}

function activateElement(element) {
  element.classList.add('active')
  setTimeout(function() {
    element.classList.remove('active')
  }, 50);
}

function getVarNbr(rgbColor) {
  makeColorList()
  let needleHex = allToHex(rgbColor)
  for (var i = 0; i < colors.length; i++) {
    closerColor = getClosestColor(needleHex)
    if (closerColor == colors[i].hex) { // if element’s color matches a line
      let cssVar = colors[i].var.replace("--c", "") // get number only
      return cssVar
    }
  }
}

function makeColorList() {
  colors = [] // empty the color array
  let vars = getColorCSSVars() // get colorvar and colorname
  for (var i = 0; i < vars.length; i++) {
    let colorVar = vars[i][0] // retrive var from getColorCSSVars
    let colorName = vars[i][1] // retrive name from getColorCSSVars
    let hex = allToHex(colorName)
    let color = {} // create color object
    color.name = colorName
    color.var = colorVar
    color.hex = hex
    colors.push(color) // add the color to colors array
  }
}

// Get applied CSS variables
const isSameDomain = (styleSheet) => {
  if (!styleSheet.href) {
    return true;
  }
  return styleSheet.href.indexOf(window.location.origin) === 0;
};
const isStyleRule = (rule) => rule.type === 1;
const getColorCSSVars = () => [...document.styleSheets].filter(isSameDomain).reduce(
  (finalArr, sheet) =>
  finalArr.concat(
    [...sheet.cssRules].filter(isStyleRule).reduce((propValArr, rule) => {
      const props = [...rule.style]
        .map((propName) => [
          propName.trim(),
          rule.style.getPropertyValue(propName).trim()
        ])
        .filter(([propName]) => propName.indexOf("--c") === 0);

      return [...propValArr, ...props];
    }, [])
  ),
  []
);

function reportWindowSize() {
  wh = window.innerHeight
  ww = window.innerWidth
}


// Utils
function generatePattern(beats, steps) {
  // Bjorklund algorithm from https://gist.github.com/withakay/1286731
  steps = Math.round(steps)
  beats = Math.round(beats)
  if (beats > steps || beats == 0 || isNaN(beats) || steps == 0 || isNaN(steps)) {
    console.info('Element needs width / height');
    return []
  }
  let pattern = []
  let counts = []
  let remainders = []
  let divisor = steps - beats
  remainders.push(beats)
  let level = 0
  while (true) {
    counts.push(Math.floor(divisor / remainders[level]))
    remainders.push(divisor % remainders[level])
    divisor = remainders[level]
    level += 1
    if (remainders[level] <= 1) {
      break
    }
  }
  counts.push(divisor)
  let r = 0
  const build = function(level) {
    r += 1
    if (level > -1) {
      for (var i = 0; i < counts[level]; i++) {
        build(level - 1)
      }
      if (remainders[level] !== 0) {
        build(level - 2)
      }
    } else if (level === -1) {
      pattern.push(0)
    } else if (level === -2) {
      pattern.push(1)
    }
  }
  build(level)
  return pattern.reverse()
}

function RGBtoHSL(rgb) {
  // https://css-tricks.com/converting-color-spaces-in-javascript/
  // If rgba, the color is still considered valid but the a isn't taken into consideration
  let ex = /^rgb\((((((((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5]),\s?)){2}|((((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5])\s)){2})((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5]))|((((([1-9]?\d(\.\d+)?)|100|(\.\d+))%,\s?){2}|((([1-9]?\d(\.\d+)?)|100|(\.\d+))%\s){2})(([1-9]?\d(\.\d+)?)|100|(\.\d+))%))\)$/i;
  let ex_rgba = /^rgba\((((((((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5]),\s?)){3})|(((([1-9]?\d(\.\d+)?)|100|(\.\d+))%,\s?){3}))|(((((1?[1-9]?\d)|10\d|(2[0-4]\d)|25[0-5])\s){3})|(((([1-9]?\d(\.\d+)?)|100|(\.\d+))%\s){3}))\/\s)((0?\.\d+)|[01]|(([1-9]?\d(\.\d+)?)|100|(\.\d+))%)\)$/i;
  if (ex.test(rgb) || ex_rgba.test(rgb)) {
    let sep = rgb.indexOf(",") > -1 ? "," : " ";
    if (ex.test(rgb)) {
      rgb = rgb.substr(4).split(")")[0].split(sep);
    } else {
      rgb = rgb.substr(5).split(")")[0].split(sep);

    }
    for (let R in rgb) {
      let r = rgb[R];
        if (r.indexOf("%") > -1)
          rgb[R] = Math.round(r.substr(0, r.length - 1) / 100 * 255);
    }

    let r = rgb[0] / 255,
      g = rgb[1] / 255,
      b = rgb[2] / 255,
      cmin = Math.min(r, g, b),
      cmax = Math.max(r, g, b),
      delta = cmax - cmin,
      h = 0,
      s = 0,
      l = 0;
    if (delta == 0)
      h = 0;
    else if (cmax == r)
      h = ((g - b) / delta) % 6;
    else if (cmax == g)
      h = (b - r) / delta + 2;
    else
      h = (r - g) / delta + 4;
    h = Math.round(h * 60);
    if (h < 0)
      h += 360;
    l = (cmax + cmin) / 2;
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);
    return {
      'hue': h,
      'sat': s,
      'lum': l
    }
  } else {
    console.info('Invalid color');
    return {
      'hue': 100,
      'sat': 100,
      'lum': 100
    };
  }
}

function allToHex(colorStr) {
  var a = document.createElement('div');
  a.style.color = colorStr;
  var colors = window.getComputedStyle(document.body.appendChild(a)).color.match(/\d+/g).map(function(a) {
    return parseInt(a, 10);
  });
  document.body.removeChild(a);
  return (colors.length >= 3) ? '#' + (((1 << 24) + (colors[0] << 16) + (colors[1] << 8) + colors[2]).toString(16).substr(1)) : false;
}

function copyNodeStyle(sourceNode, targetNode) {
  const computedStyle = window.getComputedStyle(sourceNode);
  Array.from(computedStyle).forEach(key => targetNode.style.setProperty(key, computedStyle.getPropertyValue(key), computedStyle.getPropertyPriority(key)))
}

function message(content) {
  let messageBox
  if (d.querySelector('.message-box') == undefined) {
    messageBox = d.createElement('div')
    messageBox.classList.add('message-box')
    b.appendChild(messageBox)
  } else {
    messageBox = d.querySelector('.message-box')
  }
  messageBox.innerHTML = content
  setTimeout(function() {
    messageBox.remove()
  }, 5000);
}

function getRatio(a, b) {
  if (isNaN(a) || isNaN(b)) {
    return [0, 0]    
  }else{
    return [a / gcd(a, b), b / gcd(a, b)]
  }
}

function gcd(a, b) { // greatest common divisor
  return (b == 0) ? a : gcd(b, a % b);
}

function createStartBtn() {
  let startBtn = d.createElement('button')
  startBtn.classList.add('start')
  startBtn.innerHTML = "start"
  startBtn.addEventListener('click', () => activate())
  b.appendChild(startBtn)
}

function activate() {
  Tone.Transport.start()
  init()
  d.querySelector('.start').remove()
}
function shiftPat(pattern, shiftVal){
  while (shiftVal > 0) {
    let last = pattern.pop()
    pattern.unshift(last)
    shiftVal--
  }
}

function createColorTable() {
  if (d.querySelector('.color-scale')) {
    d.querySelector('.color-scale').remove()
  }
  let scale = d.createElement('div')
  scale.classList.add('color-scale')
  b.appendChild(scale)
  for (var i = 0; i < colors.length; i++) {
    let colorBox = d.createElement('div')
    colorBox.classList.add('color-box')
    colorBox.style.backgroundColor = colors[i].hex
    colorBox.setAttribute('data-var', colors[i].var)
    if (!instruments[i] || instruments[i] == 'undefined') {
      colorBox.setAttribute('data-name', "undef")
    } else {
      colorBox.setAttribute('data-name', instruments[i].name)
    }
    scale.appendChild(colorBox)
  }
}

function initTimer() {
  timerMin = 0
  let timer = d.createElement('div')
  timer.classList.add('c-timer')
  timer.innerHTML = timerMin
  b.appendChild(timer)
  setInterval(() => {
    timerMin++
    timer.innerHTML = timerMin
    console.info('⧗ '+timerMin)
  }, 60000);
}

function getClosestColor(hex) {
  let stats = []
  for (var i = 0; i < colors.length; i++) {
    let color = {}
    color.hex = colors[i].hex
    color.score = hexColorDelta(colors[i].hex, hex)
    stats.push(color)
  }
  stats = stats.sort((a, b) => b.score - a.score)
  let bestScore = stats[0].hex
  return bestScore
}

function hexColorDelta(hex1, hex2) {
  hex1 = hex1.replace("#", "")
  hex2 = hex2.replace("#", "")
  var r1 = parseInt(hex1.substring(0, 2), 16);
  var g1 = parseInt(hex1.substring(2, 4), 16);
  var b1 = parseInt(hex1.substring(4, 6), 16);
  // get red/green/blue int values of hex2
  var r2 = parseInt(hex2.substring(0, 2), 16);
  var g2 = parseInt(hex2.substring(2, 4), 16);
  var b2 = parseInt(hex2.substring(4, 6), 16);
  // calculate differences between reds, greens and blues
  var r = 255 - Math.abs(r1 - r2);
  var g = 255 - Math.abs(g1 - g2);
  var b = 255 - Math.abs(b1 - b2);
  // limit differences between 0 and 1
  r /= 255;
  g /= 255;
  b /= 255;
  // 0 means opposit colors, 1 means same colors
  return (r + g + b) / 3;
}
const getRealCssVal = (el, property) => {
  // Crome only
  // let styleMap = el.computedStyleMap();
  // const unitvalue = styleMap.get(property);

  // Firefox compatible
  const unitvalue = getComputedStyle(el).getPropertyValue(property)
  if (unitvalue.unit == undefined) { // avoid fatal js error
    return "0px"
  }
  return unitvalue.value
}
